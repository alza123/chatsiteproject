# Generated by Django 4.0 on 2021-12-29 10:38

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('chatapp', '0004_alter_message_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2021, 12, 29, 10, 38, 26, 168129, tzinfo=utc)),
        ),
    ]
